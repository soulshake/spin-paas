#!/usr/bin/env python
from __future__ import print_function
import xmlrpclib
import sys
import os
import random
import time
import base64
import sha
 
api = xmlrpclib.ServerProxy('https://rpc.gandi.net/xmlrpc/')
apikey = os.environ['GANDI_API_KEY']

adjectives = ['thirsty', 'giggly', 'squirrely', 'grumbly', 'hungry', 
'slippery', 'beloved', 'trolled', 'bubbly', 'fuzzy', 'crazy', 'green', 
'tasty', 'sleepy', 'icy', 'evil', 'crunchy', 'curly', 'purple']

animals = ['panda', 'squirrel', 'kitten', 'octopus', 'elephant', 'pony', 
'giraffe', 'unicorn', 'duck', 'monkey']

def generate_hostname():
    return '-'.join(random.sample(adjectives, 1) + random.sample(animals, 1))

def generate_password(instance_name):
    secret_prefix = 'aniania-'
    password_length = 8
    secret = secret_prefix + instance_name
    digest = sha.sha(secret).digest()
    alphanum = base64.b32encode(digest)
    return alphanum[:password_length]

def create_instance(event, hostname):
    fqdn = hostname + '.gandi.xyz'
    print("Creating {}... ".format(fqdn, end=" "))
    paas_spec = {
        'name': event + '-' + hostname,
        'size': 's',
        'type': 'pythonmysql',
        'datacenter_id': 2,
        'keys': [872, 1432],
        'quantity': 0,
        'duration': '1m',
        'vhosts': [fqdn], 
        'password': generate_password(hostname)}
    
    assert fqdn == paas_spec['vhosts'][0]

    ops = api.paas.create(apikey, paas_spec)
    op_0, op_1, op_2 = ops
    paas_id = op_0['params']['paas_id']
    paas_info = api.paas.info(apikey, paas_id)
    print("---> {} (id {}) creation launched!".format(hostname, paas_info['id']))
    return ops

def get_our_instances(event):
    instance_list = api.paas.list(apikey)

    instance_list = [ i for i in instance_list
        if i['name'].startswith(event) 
        and i['state'] == 'running' ]
    return instance_list

def list_instances(event):
    # print a list of existing instances and connection information
    for instance in get_our_instances(event):
        i = api.paas.info(apikey, instance['id'])
        hostname = i['name'].split('-', 1)[1]
        fqdn = hostname + '.gandi.xyz' # AKA vhost
        
        records = api.paas.vhost.get_dns_entries(
                    apikey, {'datacenter': 2, 'vhost': fqdn})
        ipv4 = records['ipv4']
        ipv6 = records['ipv6']
        cname = records['cname']
        name = i['name']
        instance_id = i['id']
        user = i['user'] # same for git, sftp, db login, admin
        password = generate_password(hostname)
        git_server = i['git_server'] # git.dc1.gpaas.net
        ftp_server = i['ftp_server'] # sftp.dc1.gpaas.net
        console_server = i['console'].split('@', 1)[1] # console.dc1.gpaas.net
        php_admin_user = "root"
        php_admin_password = ""
        # git+ssh://174202@git.dc1.gpaas.net/{vhost}.git
        # sftp user@sftp.dc1.gpaas.net
        # ssh user@console.dc1.gpaas.net
        print(instance_id, "  name:  ", name)
        print(instance_id, "  fqdn:  ", fqdn)
        print(instance_id, "  user:  ", user)
        print(instance_id, "password: ", password)
        print(instance_id, "  git:   ", git_server)
        print(instance_id, "  sftp:  ", ftp_server)
        print(instance_id, "  ipv4:  ", ipv4)
        print(instance_id, "  ipv6:  ", ipv6)
        print(instance_id, " cname: ", cname)
        print("Connection info:")
        print("  git remote add origin ssh+git://{login}@git.{dc}.gpaas.net/{vhost}.git"
            .format(login=user, dc='dc1', vhost=i['vhosts'][0]['name']))
        print("  sftp {user}@sftp.{dc}.gpaas.net".format(user=user, dc='dc1'))
        print("  ssh {user}@console.{dc}.gpaas.net".format(user=user, dc='dc1'))
        print("--------------------------------------------")

def activate_ssh(paas_id):
    # activate ssh console on a single instance
    op = api.paas.update(apikey, paas_id, {'console' : 'True'})

def activate_ssh_all(event):
    # activate ssh console on all instances
    instance_list = api.paas.list(apikey)
    instance_list = [ i for i in instance_list
        if i['name'].startswith(event) ]

    ops = [ activate_ssh(inst['id']) for inst in get_our_instances(event) ]

def delete_instances(event):
    # delete all the python instances that aren't stuck
    instance_list = api.paas.list(apikey)
    print("Deleting {} instances.".format(len(instance_list)))
    
    ops = [api.paas.delete(apikey, inst['id']) 
           for inst in instance_list 
           if inst['catalog_name'] == 'pythonmysql_s' 
              and inst['id'] != 104036]
    return ops

def update_zone(hostname, fqdn, new_zone_version):
    # Add a resource record to the zone file

    dns_entries = api.paas.vhost.get_dns_entries(
                    apikey, 
                    {'datacenter': 2, 'vhost': fqdn})

    record = {
        'ttl': 1800,
        'type': 'CNAME',
        'name': hostname,
        'value': dns_entries['cname'],
        }

    api.domain.zone.record.add(apikey, zone_id, new_zone_version, record)
        
def dns_create_records(event):
    # update zone for all existing instances 
    zone_id = 1477280 # carrots.gandi.xyz  
    api.domain.zone.version.set(apikey, zone_id, 4)
    new_zone_version = api.domain.zone.version.new(apikey, zone_id)
    instance_list = api.paas.list(apikey)
    instance_list = [ i for i in instance_list
        if i['name'].startswith(event) ]

    for instance in instance_list:
        i = api.paas.info(apikey, instance['id'])
        hostname = i['name'].split('-', 1)[1]
        fqdn = hostname + '.gandi.xyz' # AKA vhost

        print("Updating zone: {}, {}, version {}"
            .format(hostname, fqdn, new_zone_version))
        update_zone(hostname, fqdn, new_zone_version)
    # set active version of zone
    print("Activating zone {}, version {}".format(zone_id, new_zone_version))
    api.domain.zone.version.set(apikey, zone_id, new_zone_version)

if __name__ == '__main__':
    event = 'pycon'
    domain = 'gandi.xyz'
    zone_id = 1477280 # carrots.gandi.xyz  
    zone_info = api.domain.zone.info(apikey, zone_id)

    action = sys.argv[1]
    domain_info = api.domain.info(apikey, domain)

    if action == 'ssh':
        activate_ssh(int(sys.argv[2]))

    if action == 'ssh-all':
        activate_ssh_all(event)

    if action == 'create':
        quantity = int(sys.argv[2])

    if action == 'delete':
        delete_instances(event)

    if action == 'list':
        list_instances(event)

    if action == 'dns':
        dns_create_records(event)

    if action == 'create':
        instances = [ i['name'] for i in get_our_instances(event) ]
        print(instances)

        for i in range(quantity):
            hostname = generate_hostname()
            while hostname in instances:
                hostname = generate_hostname()
                
            instances.append('pycon-' + hostname)
            fqdn = hostname + '.' + domain
            ops = create_instance(event, hostname) # 3 dicts of operations
            print([op['id'] for op in ops])
            # while at least 1 op is in RUN, sleep
            while not all([ api.operation.info(apikey, op['id'])['step'] == "DONE" 
                            for op in ops ]):
                print('...', end=" ")
                print([ api.operation.info(apikey, x['id'])['step'] for x in ops ])
                time.sleep(9)
        

